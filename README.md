Distributors to the Network, Data, Electrical, AV and Telecoms Industry for your Cable Management, Containment and Networking products.
You’ll be treated to a reliable, open and honest service, whether you have a multimillion-pound contract or just need a few fibreoptic patch leads.


Address: Unit 5 & 6, Arkwright Industrial Estate, Arkwright Rd, Bedford MK42 0LQ, UK

Phone: +44 1234 848030

Website: https://www.cmwltd.co.uk
